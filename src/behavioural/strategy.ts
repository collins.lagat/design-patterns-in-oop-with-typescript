interface Sort {
  sort(list: string[]): void;
}

class AscendingSort implements Sort {
  sort(list: string[]): void {
    list.sort();
  }
}

class DescendingSort implements Sort {
  sort(list: string[]): void {
    list.sort(this.compareFunction);
  }

  private compareFunction(first: string, second: string) {
    if (first.charAt(0) < second.charAt(0)) return 1;

    if (first.charAt(0) > second.charAt(0)) return -1;

    return 0;
  }
}

class Context {
  constructor(private sortStrategy: Sort, private list: string[]) {}

  setSortStrategy(strategy: Sort) {
    this.sortStrategy = strategy;
  }

  sortList() {
    this.sortStrategy.sort(this.list);
  }

  printList() {
    console.log(this.list);
  }
}

const months = ["March", "Feb", "Dec", "Jan"];
const ascStr = new AscendingSort();
const descStr = new DescendingSort();
const demoContext = new Context(ascStr, months);

console.log("Original List");
demoContext.printList();
/*
Original List
[ 'March', 'Feb', 'Dec', 'Jan' ]
*/

console.log("Ascending");
demoContext.sortList();
demoContext.printList();
/*
Ascending
[ 'Dec', 'Feb', 'Jan', 'March' ]
*/

console.log("Descending");
demoContext.setSortStrategy(descStr);
demoContext.sortList();
demoContext.printList();
/*
Descending
[ 'March', 'Jan', 'Feb', 'Dec' ]
*/
