interface IEngine {
  getType: () => string;
}

abstract class EngineFactory {
  abstract makeEngine(): IEngine;
}

// V8
class v8Engine implements IEngine {
  private engine = "v8";
  getType(): string {
    console.log(`This is a ${this.engine}.`);
    return this.engine;
  }
  toString() {
    return this.engine;
  }
}

class V8EngineFactory extends EngineFactory {
  makeEngine() {
    return new v8Engine();
  }
}

// V4
class v4Engine implements IEngine {
  private engine = "v4";
  getType(): string {
    console.log(`This is a ${this.engine}.`);
    return this.engine;
  }
  toString() {
    return this.engine;
  }
}

class V4EngineFactory extends EngineFactory {
  makeEngine() {
    return new v4Engine();
  }
}

class CarDemo {
  private engine: IEngine | null = null;

  constructor(private model: string) {
    console.log(`${model} created with no engine!`);
  }

  setEngine(engine: IEngine) {
    console.log(`Dropping in a new ${engine} engine.`);
    this.engine = engine;
  }
  toString() {
    return `A ${this.model} model with a ${this.engine} engine.`;
  }
}

const v8Fac = new V8EngineFactory();
const v4Fac = new V4EngineFactory();

const v8 = v8Fac.makeEngine();
const v4 = v4Fac.makeEngine();

const bmw = new CarDemo("BMW 3");
// BMW 3 created with no engine!

bmw.setEngine(v4);
console.log(`${bmw}`);

/**
Dropping in a new v4 engine.
A BMW 3 model with a v4 engine.
*/

bmw.setEngine(v8);
console.log(`${bmw}`);
/**
Dropping in a new v8 engine.
A BMW 3 model with a v8 engine.
*/
