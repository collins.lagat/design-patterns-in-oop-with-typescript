class Captain {
  private static instance: Captain;
  private static numberOfInstance = 0;

  private constructor() {}

  static getCaptain() {
    if (!this.instance) {
      this.instance = new Captain();
      this.numberOfInstance++;
    }
    console.log(`Number of instances at this moment=${this.numberOfInstance}`);
    return this.instance;
  }
}

// const captain1 = new Captain() // Error
const captain1 = Captain.getCaptain();
const captain2 = Captain.getCaptain();

if (captain1 === captain2) {
  console.log("captain1 and captain2 are same instance.");
}
