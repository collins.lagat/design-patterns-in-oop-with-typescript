// A Bookstore's existing System
interface Book {
  price: number;
  copies: number;
  weight: number;
  transportCost(): number;
  storageCost(): number;
}

class OrdinaryBook implements Book {
  constructor(
    public price: number,
    public copies: number,
    public weight: number
  ) {}

  transportCost(): number {
    return ((this.copies * this.weight) / 1000) * 8;
  }

  storageCost(): number {
    return this.copies * 3;
  }
}

class BookProfitCalculator {
  getGrossProfit(books: Book[]) {
    let sales = 0;
    let costs = 0;

    books.forEach((book) => {
      sales = sales + book.price * book.copies;
      costs = costs + book.storageCost() + book.transportCost();
    });

    const grossProfit = sales - costs;

    // round to 2 decimal places: https://stackoverflow.com/questions/11832914/round-to-at-most-2-decimal-places-only-if-necessary
    return Math.round((grossProfit + Number.EPSILON) * 100) / 100;
  }
}

// After Bookstore started selling Ebooks to existing customers

interface EBook {
  price: number;
  downloads: number;
  size: number;
  bandwidthCost(): number;
  hostingCost(): number;
}

class ElectronicBook implements EBook {
  constructor(
    public price: number,
    public downloads: number,
    public size: number
  ) {}

  bandwidthCost(): number {
    return this.downloads * this.size * 0.05;
  }

  hostingCost(): number {
    return this.size * 0.003;
  }
}

// Adapter for ElectronicBook
class EbookAdapter implements Book {
  price: number = this.eBook.price;
  copies: number = this.eBook.downloads;
  weight: number = this.eBook.size;

  constructor(private eBook: EBook) {}

  transportCost(): number {
    return this.eBook.bandwidthCost();
  }
  storageCost(): number {
    return this.eBook.hostingCost();
  }
}

// Demo

// Ordinary books
const ordinaryBook1 = new OrdinaryBook(9.99, 5000, 445);
const ordinaryBook2 = new OrdinaryBook(19.9, 800, 300);
const ordinaryBook3 = new OrdinaryBook(25, 11000, 650);

// Ebooks
const eBook1 = new ElectronicBook(9.99, 700, 0.1);
const eBook2 = new ElectronicBook(4.5, 2000, 0.1);
const eBook3 = new ElectronicBook(11.99, 2800, 0.4);

// Adapted Ebooks
const adaptedEbook1 = new EbookAdapter(eBook1);
const adaptedEbook2 = new EbookAdapter(eBook2);
const adaptedEbook3 = new EbookAdapter(eBook3);

// Calculate Gross Profit
const calculator = new BookProfitCalculator();

// Gross Profit: Ordinary Books
console.log(
  calculator.getGrossProfit([ordinaryBook1, ordinaryBook2, ordinaryBook3])
); // 213550

// Gross Profit: Ebooks
console.log(
  calculator.getGrossProfit([adaptedEbook1, adaptedEbook2, adaptedEbook3])
); // 49495.5

// Gross Profit: All Books
console.log(
  calculator.getGrossProfit([
    ordinaryBook1,
    ordinaryBook2,
    ordinaryBook3,
    adaptedEbook1,
    adaptedEbook2,
    adaptedEbook3,
  ])
); // 263045.5
