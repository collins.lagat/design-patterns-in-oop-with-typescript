interface IObserver {
  update: (value: string) => void;
}

interface ISubject<T> {
  register: (observer: IObserver) => void;
  unregister: (observer: IObserver) => void;
  notify: (value: T) => void;
}

class PlayerObserver implements IObserver {
  constructor(private name: string) {}
  update(instruction: string): void {
    console.log(`${this.name} was notified, with ${instruction}!`);
  }
  toString() {
    return this.name;
  }
}

class CoachSubject implements ISubject<string> {
  private players: IObserver[] = [];
  register(observer: IObserver): void {
    this.players.push(observer);
    console.log(`${observer} was added.`);
  }
  unregister(observer: IObserver): void {
    console.log(`Before: ${this.players.length}.`);
    for (let index = 0; index < this.players.length; index++) {
      if (observer === this.players[index]) {
        this.players.splice(index, 1);
        console.log(`${observer} was removed.`);
        break;
      }
    }
    console.log(`After: ${this.players.length}.`);
  }
  notify(instruction: string): void {
    this.players.forEach((player) => player.update(instruction));
  }
}

const coach = new CoachSubject();

const player1 = new PlayerObserver("John");
const player2 = new PlayerObserver("James");
const player3 = new PlayerObserver("Dave");
const player4 = new PlayerObserver("Donald");

coach.register(player1);
coach.register(player2);
coach.register(player3);
coach.register(player4);

/*
John was added.
James was added.
Dave was added.
Donald was added.
*/

coach.notify("Play Wide");

/*
John was notified, with Play Wide!
James was notified, with Play Wide!
Dave was notified, with Play Wide!
Donald was notified, with Play Wide!
*/

coach.unregister(player1);
/*
Before: 4.
John was removed.
After: 3.
*/

coach.notify("Tiki Taka");

/**
James was notified, with Tiki Taka!
Dave was notified, with Tiki Taka!
Donald was notified, with Tiki Taka!
*/
