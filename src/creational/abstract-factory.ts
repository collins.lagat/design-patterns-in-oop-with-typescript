// Enumns

enum Elements {
  BEAM,
  COLUMN,
}

enum Beams {
  I_BEAM,
  S_BEAM,
}

enum Columns {
  I_COLUMN,
  S_COLUMN,
}

// Abstract Element Classes

interface IElement {
  name: string;
}

interface IBeam extends IElement {}

interface IColumn extends IElement {}

// Concrete Beam Classes

class SquareSectionBeam implements IBeam {
  constructor(public name: string = "Square Beam") {}
}

class ISectionBeam implements IBeam {
  constructor(public name: string = "I Beam") {}
}

// Concrete Column Classes

class SquareSectionColumn implements IColumn {
  constructor(public name: string = "Square Column") {}
}

class ISectionColumn implements IColumn {
  constructor(public name: string = "I Column") {}
}

// Abstract Factory Class

abstract class ElementFactory {
  abstract getElement(element: Beams | Columns): IElement;
  static getInstance: () => ElementFactory;
}

// Concrete Factory Classes

class BeamFactory implements ElementFactory {
  static getInstance(): BeamFactory {
    let _instance: BeamFactory | null = null;
    if (_instance) {
      return _instance;
    }
    _instance = new BeamFactory();

    return _instance;
  }

  getElement(beam: Beams): IBeam {
    let _beam: IBeam | null = null;

    switch (beam) {
      case Beams.I_BEAM:
        _beam = new ISectionBeam();
        break;
      case Beams.S_BEAM:
        _beam = new SquareSectionBeam();
        break;

      default:
        throw new Error("Null Object");
    }

    return _beam;
  }
}

class ColumnFactory implements ElementFactory {
  static getInstance(): ColumnFactory {
    let _instance: ColumnFactory | null = null;
    if (_instance) {
      return _instance;
    }
    _instance = new ColumnFactory();

    return _instance;
  }

  getElement(beam: Columns): IColumn {
    let _column: IColumn | null = null;

    switch (beam) {
      case Columns.I_COLUMN:
        _column = new ISectionColumn();
        break;
      case Columns.S_COLUMN:
        _column = new SquareSectionColumn();
        break;

      default:
        throw new Error("Null Object");
    }

    return _column;
  }
}

// Factory Producer

class StructuralElementFactory {
  getFactory(element: Elements): ElementFactory {
    let _factory: ElementFactory | null = null;
    switch (element) {
      case Elements.BEAM:
        _factory = BeamFactory.getInstance();
        break;
      case Elements.COLUMN:
        _factory = ColumnFactory.getInstance();
        break;

      default:
        throw new Error("Null Object");
    }
    return _factory;
  }
}

// Demo

const abstractFactory = new StructuralElementFactory();

const beamFactory = abstractFactory.getFactory(Elements.BEAM);
const columnFactory = abstractFactory.getFactory(Elements.COLUMN);

const beam1 = beamFactory.getElement(Beams.I_BEAM),
  beam2 = beamFactory.getElement(Beams.I_BEAM);

const column1 = columnFactory.getElement(Columns.S_COLUMN),
  column2 = columnFactory.getElement(Columns.S_COLUMN);

console.log(beam1, beam2, column1, column2);
