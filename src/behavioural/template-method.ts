class Burger {
  bun: string = "NOT SET";
  patty: string = "NOT SET";
  cheese: string = "NOT SET";

  setBun(bun: string) {
    this.bun = bun;
  }

  setPatty(patty: string) {
    this.patty = patty;
  }

  setCheese(cheese: string) {
    this.cheese = cheese;
  }

  toString() {
    return `
Burger With:
- Bun: ${this.bun}
- Patty: ${this.patty}
- cheese: ${this.cheese}
      `;
  }
}

abstract class MakeBurger {
  burger: Burger;

  constructor() {
    this.burger = new Burger();
  }

  makeBurger() {
    this.addBun();
    this.addPatty();
    this.addCheese();
    return this.burger;
  }

  abstract addBun(): void;

  abstract addPatty(): void;

  abstract addCheese(): void;
}

class MakeFrenchConnection extends MakeBurger {
  addBun(): void {
    this.burger.setBun("Croissant");
  }
  addPatty(): void {
    this.burger.setPatty("Garlic, Medium Rare");
  }
  addCheese(): void {
    this.burger.setCheese("Blue Cheese");
  }
}

class MakeTexasBurger extends MakeBurger {
  addBun(): void {
    this.burger.setBun("Brioche");
  }
  addPatty(): void {
    this.burger.setPatty("Onion, Medium");
  }
  addCheese(): void {
    this.burger.setCheese("Provolone");
  }
}

class MakeSubwayBurger extends MakeBurger {
  addBun(): void {
    this.burger.setBun("Baguette");
  }
  addPatty(): void {
    this.burger.setPatty("Pulled Chicken");
  }
  addCheese(): void {
    this.burger.setCheese("American Cheese");
  }
}

class MakeKoreanBurger extends MakeBurger {
  addBun(): void {
    this.burger.setBun("Milk Bread Roll");
  }
  addPatty(): void {
    this.burger.setPatty("Pineapple, Medium Rare");
  }
  addCheese(): void {
    this.burger.setCheese("Swiss Cheese");
  }
}

const burgers: Burger[] = [];

burgers.push(new MakeFrenchConnection().makeBurger());
burgers.push(new MakeTexasBurger().makeBurger());
burgers.push(new MakeSubwayBurger().makeBurger());
burgers.push(new MakeKoreanBurger().makeBurger());

burgers.forEach((burger) => console.log(burger.toString()));
