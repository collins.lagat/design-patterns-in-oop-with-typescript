export interface ISection {
  length: number;
  area: number;
  secondMomentOfArea: number;
}

export interface IMaterial {
  designStrength: number;
}

export interface IBeam<K, L> {
  section: K;
  material: L;
}

class Beam implements IBeam<ISection, IMaterial> {
  section: ISection = {
    length: 0,
    area: 0,
    secondMomentOfArea: 0,
  };
  material: IMaterial = {
    designStrength: 0,
  };
}

abstract class Builder {
  newBeam(): void {}
  addSection(): void {}
  addMaterial(): void {}
  getBeam() {}
}

class SteelBeamBuilder extends Builder {
  private _beam: Beam | null = null;

  newBeam() {
    this._beam = new Beam();
  }

  addMaterial() {
    this._beam!.material = {
      designStrength: 275,
    };
  }

  addSection() {
    this._beam!.section = {
      length: 50,
      area: 105,
      secondMomentOfArea: 47500,
    };
  }

  getBeam(): Beam {
    return this._beam!;
  }
}

class ConcreteBeamBuilder extends Builder {
  private _beam: Beam | null = null;

  newBeam() {
    this._beam = new Beam();
  }

  addMaterial() {
    this._beam!.material = {
      designStrength: 30,
    };
  }

  addSection() {
    this._beam!.section = {
      length: 50,
      area: 105,
      secondMomentOfArea: 47500,
    };
  }

  getBeam(): Beam {
    return this._beam!;
  }
}

class ElementDirector {
  private _builder: Builder | null = null;

  setBuilder(builder: Builder) {
    this._builder = builder;
  }

  buildBeam() {
    this._builder?.newBeam();
    this._builder?.addMaterial();
    this._builder?.addSection();
    return this._builder?.getBeam();
  }
}

const director = new ElementDirector();

const steelBuilder = new SteelBeamBuilder();

director.setBuilder(steelBuilder);

const beam1 = director.buildBeam();

const concreteBuilder = new ConcreteBeamBuilder();

director.setBuilder(concreteBuilder);

const beam2 = director.buildBeam();

console.log(beam1, beam2);
